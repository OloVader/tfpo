package pucrs.myflight.modelo;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class GerenciadorCias implements Iterable<CiaAerea> {
	private ArrayList<CiaAerea> empresas;

	public GerenciadorCias() {
		empresas = new ArrayList<>();
	}

	public void adicionar(CiaAerea cia) {
		empresas.add(cia);
	}

	public void carregaDados() throws IOException {
		Path path1 = Paths.get("airlines.dat");
		try (Scanner sc = new Scanner(Files.newBufferedReader(path1, Charset.forName("utf8")))) {
			sc.useDelimiter("[;\n]"); // separadores: ; e nova linha
			String header = sc.nextLine(); // pula cabeçalho
			String id, nome;
			while (sc.hasNext()) {
				id = sc.next();
				nome = sc.next();
				System.out.format("%s - %s%n", id, nome);
				empresas.add(new CiaAerea(id, nome));
			}
		}
	}

	public ArrayList<CiaAerea> listarTodas() {
		// ArrayList<CiaAerea> nova = new ArrayList<>();
		// for(CiaAerea cia: empresas)
		// nova.add(cia);
		// return nova;
		return new ArrayList<CiaAerea>(empresas);
	}

	public CiaAerea buscarCodigo(String codigo) {
		for (CiaAerea c : empresas) {
			if (codigo.equals(c.getCodigo()))
				return c;
		}
		return null; // não achamos!
	}

	public CiaAerea buscarNome(String nome) {
		for (CiaAerea c : empresas) {
			if (nome.equals(c.getNome()))
				return c;
		}
		return null; // não achamos!
	}

	@Override
	public Iterator<CiaAerea> iterator() {
		// TODO Auto-generated method stub
		return new Iterator<CiaAerea>() {

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public CiaAerea next() {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}
}
